package com.hmehta.aws.s3;

import java.util.List;
import java.util.Arrays;
import org.slf4j.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.http.HttpHeaders;
import com.amazonaws.auth.BasicAWSCredentials;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.springframework.web.multipart.MultipartFile;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import org.springframework.beans.factory.annotation.Value;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@Component
@EnableAutoConfiguration
public class S3Wrapper {

	private AmazonS3Client amazonS3Client;

	@Value("${cloud.aws.s3.bucket}")
	private String bucket;

	@Value("${cloud.aws.access.key}")
	private String awsAccessKey;

	@Value("${cloud.aws.secret.key}")
	private String awsSecrectKey;

	private Logger logger = org.slf4j.LoggerFactory.getLogger(S3Wrapper.class);

	private PutObjectResult upload(InputStream inputStream, String uploadKey) {
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, uploadKey, inputStream, new ObjectMetadata());

		putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);

		amazonS3Client = new AmazonS3Client(new BasicAWSCredentials(awsAccessKey, awsSecrectKey));
		PutObjectResult putObjectResult = amazonS3Client.putObject(putObjectRequest);
		IOUtils.closeQuietly(inputStream);

		return putObjectResult;
	}

	public List<PutObjectResult> upload(MultipartFile[] multipartFiles) {
		List<PutObjectResult> putObjectResults = new ArrayList<>();
		Arrays.stream(multipartFiles).filter(multipartFile -> !StringUtils.isEmpty(multipartFile.getOriginalFilename())).forEach(multipartFile -> {
			try {
				String namePrefix = Long.toString(new GregorianCalendar().getTimeInMillis());
				String nameSuffix = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."),
						multipartFile.getOriginalFilename().length());
				putObjectResults.add(upload(multipartFile.getInputStream(), namePrefix + nameSuffix));
				putObjectResults.get(0).getMetadata().addUserMetadata("fileName", namePrefix + nameSuffix);
				logger.info("Uploaded file=" + putObjectResults.get(0).getMetadata().getUserMetaDataOf("fileName"));
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		});

		return putObjectResults;
	}

	public ResponseEntity<byte[]> download(String key) throws IOException {
		GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, key);

		S3Object s3Object = amazonS3Client.getObject(getObjectRequest);

		S3ObjectInputStream objectInputStream = s3Object.getObjectContent();

		byte[] bytes = IOUtils.toByteArray(objectInputStream);

		String fileName = URLEncoder.encode(key, "UTF-8").replaceAll("\\+", "%20");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		httpHeaders.setContentLength(bytes.length);
		httpHeaders.setContentDispositionFormData("attachment", fileName);

		return new ResponseEntity<>(bytes, httpHeaders, HttpStatus.OK);
	}

	public List<S3ObjectSummary> list() {
		ObjectListing objectListing = amazonS3Client.listObjects(new ListObjectsRequest().withBucketName(bucket));

		List<S3ObjectSummary> s3ObjectSummaries = objectListing.getObjectSummaries();

		return s3ObjectSummaries;
	}
}