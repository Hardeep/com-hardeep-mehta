package com.hmehta.restcontrollers;

import java.util.List;
import com.hmehta.aws.s3.S3Wrapper;
import com.hmehta.config.HardeepMehtaConfiguration;

import javax.servlet.http.HttpServletRequest;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.hmehta.service.impl.VerifyRecapthchaServiceImpl;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 * Image Processor Rest Controller - Resize image + Change color mode to
 * Monochrome or Black & White
 * 
 * @author HMehta
 */
@RestController
@EnableAutoConfiguration
public class ImageProcessorRestController extends AbstractRestController {

    @Autowired
    private S3Wrapper                   s3Wrapper;

    @Autowired
    private VerifyRecapthchaServiceImpl service;

    @Autowired
    private HardeepMehtaConfiguration   configuration;

    @RequestMapping(value = "/imageprocessor", method = RequestMethod.POST, produces = { "application/json" })
    public String upload(@RequestParam(name = "imageFile", required = true) MultipartFile[] multipartFiles, @RequestParam(name = "imageWidth", required = true) String imageWidth,
            @RequestParam(name = "imageColorMode", required = true) String imageColorMode, @RequestParam(name = "g-recaptcha-response", required = true) String response, HttpServletRequest request) {

        String result = "{}";
        if (service.isUserVerified(response, request.getRemoteAddr())) {
            // Upload the file to s3 bucket
            restLogger.info("Uploading file=" + multipartFiles[0].getOriginalFilename() + ", Image (New) Width=" + imageWidth + ", Image (New) Color Mode=" + imageColorMode);
            List<PutObjectResult> results = s3Wrapper.upload(multipartFiles);

            // Call the Lambda function
            String imageName = results.get(0)
                                      .getMetadata()
                                      .getUserMetaDataOf("fileName");
            String awsLambdaRESTURL = configuration.getAwsLambdaImageProcessorFunctionUrl() + "?imageName=" + imageName + "&imageWidth=" + imageWidth + "&imageColorMode=" + imageColorMode;

            // Return JSON response, that contains the processed image location
            result = new RestTemplate().getForObject(awsLambdaRESTURL, String.class);
        }

        return result;
    }

}
