package com.hmehta.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Configuration
@ConfigurationProperties(locations = "classpath:twitter.properties", ignoreUnknownFields = false, prefix = "twitter")
public class TwitterConfiguration {

	private String consumerKey;
	private String consumerSecrect;
	private String oauthAccessToken;
	private String oauthAccessSecret;

	public String getConsumerKey() {
		return consumerKey;
	}

	public String getConsumerSecrect() {
		return consumerSecrect;
	}

	public String getOauthAccessToken() {
		return oauthAccessToken;
	}

	public String getOauthAccessSecret() {
		return oauthAccessSecret;
	}

	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}

	public void setConsumerSecrect(String consumerSecrect) {
		this.consumerSecrect = consumerSecrect;
	}

	public void setOauthAccessToken(String oauthAccessToken) {
		this.oauthAccessToken = oauthAccessToken;
	}

	public void setOauthAccessSecret(String oauthAccessSecret) {
		this.oauthAccessSecret = oauthAccessSecret;
	}
}
