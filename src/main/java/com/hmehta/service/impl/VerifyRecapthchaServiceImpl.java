package com.hmehta.service.impl;

import org.slf4j.Logger;
import java.util.Collection;
import org.springframework.util.MultiValueMap;
import org.springframework.stereotype.Component;
import com.hmehta.config.HardeepMehtaConfiguration;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.client.RestClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@Component
@EnableAutoConfiguration
public class VerifyRecapthchaServiceImpl {

    @Autowired
    private HardeepMehtaConfiguration configuration;

    private Logger                    logger = org.slf4j.LoggerFactory.getLogger(VerifyRecapthchaServiceImpl.class);

    public boolean isUserVerified(String response, String remoteIp) {
        RecaptchaResponse recaptchaResponse = null;
        try {
            recaptchaResponse = (new RestTemplate()).postForEntity(configuration.getReCapthachaUrl(), createBody(configuration.getReCapthachaSecret(), remoteIp, response), RecaptchaResponse.class).getBody();
        } catch (RestClientException e) {
            logger.error(e.toString());
        }
        if (recaptchaResponse != null) {
            return recaptchaResponse.success;
        } else {
            return false;
        }
    }

    private MultiValueMap<String, String> createBody(String secret, String remoteIp, String response) {
        MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
        form.add("secret", secret);
        form.add("remoteip", remoteIp);
        form.add("response", response);
        return form;
    }

    private static class RecaptchaResponse {
        @JsonProperty("success")
        private boolean            success;
        @JsonProperty("error-codes")
        private Collection<String> errorCodes;
    }
}
