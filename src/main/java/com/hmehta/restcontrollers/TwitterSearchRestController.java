package com.hmehta.restcontrollers;

import twitter4j.Query;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.JSONArray;
import twitter4j.JSONObject;
import twitter4j.QueryResult;
import twitter4j.JSONException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterException;
import twitter4j.Query.ResultType;
import twitter4j.conf.ConfigurationBuilder;
import com.hmehta.config.TwitterConfiguration;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@RestController
@EnableAutoConfiguration
public class TwitterSearchRestController extends AbstractRestController {

	private static Twitter twitter = null;

	@Autowired
	private TwitterConfiguration configuration;

	@RequestMapping(value = "/twittersearch")
	public String getTweets(@RequestParam(value = "hashtag", defaultValue = "NewYork") String hashtag) {

		JSONObject statuses = new JSONObject();
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true).setOAuthConsumerKey(configuration.getConsumerKey()).setOAuthConsumerSecret(configuration.getConsumerSecrect())
				.setOAuthAccessToken(configuration.getOauthAccessToken()).setOAuthAccessTokenSecret(configuration.getOauthAccessSecret())
				.setJSONStoreEnabled(true).setIncludeEntitiesEnabled(true);
		TwitterFactory twitterFactory = new TwitterFactory(cb.build());
		if (twitter == null) {
			twitter = twitterFactory.getInstance();
		}

		Query query = new Query(hashtag);
		query.setLang("en");
		query.setResultType(ResultType.recent);
		query.setCount(5);

		try {
			JSONArray array = new JSONArray();
			QueryResult result = twitter.search(query);
			for (Status status : result.getTweets()) {
				JSONObject item = new JSONObject(status);
				array.put(item);
				restLogger.info("Tweet, handel=@" + status.getUser().getScreenName() + ", text=" + status.getText());
			}
			statuses.put("statuses", array);
		} catch (TwitterException | JSONException e) {
			restLogger.error("Exception, class=" + this.getClass().getName() + ", details=" + e.toString());
		}
		return statuses.toString();
	}
}