package com.hmehta.restcontrollers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Abstract class, extended by all rest controllers
 * 
 * @author hardeep
 *
 */
public abstract class AbstractRestController {
	
	protected Logger restLogger = LoggerFactory.getLogger(this.getClass().getName());
		
}