package com.hmehta.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Configuration
@ConfigurationProperties(locations = "classpath:hardeepmehta.properties", ignoreUnknownFields = false, prefix = "hm")
public class HardeepMehtaConfiguration {
    
    private String reCapthachaUrl                     = null;
    private String reCapthachaSecret                  = null;
    private String awsLambdaSmsProcessorFunctionUrl   = null;
    private String awsLambdaImageProcessorFunctionUrl = null;

    public String getReCapthachaSecret() {
        return reCapthachaSecret;
    }

    public void setReCapthachaSecret(String reCapthachaSecret) {
        this.reCapthachaSecret = reCapthachaSecret;
    }

    public String getAwsLambdaSmsProcessorFunctionUrl() {
        return awsLambdaSmsProcessorFunctionUrl;
    }

    public void setAwsLambdaSmsProcessorFunctionUrl(String awsLambdaSmsProcessorFunctionUrl) {
        this.awsLambdaSmsProcessorFunctionUrl = awsLambdaSmsProcessorFunctionUrl;
    }

    public String getAwsLambdaImageProcessorFunctionUrl() {
        return awsLambdaImageProcessorFunctionUrl;
    }

    public void setAwsLambdaImageProcessorFunctionUrl(String awsLambdaImageProcessorFunctionUrl) {
        this.awsLambdaImageProcessorFunctionUrl = awsLambdaImageProcessorFunctionUrl;
    }

    public String getReCapthachaUrl() {
        return reCapthachaUrl;
    }

    public void setReCapthachaUrl(String reCapthachaUrl) {
        this.reCapthachaUrl = reCapthachaUrl;
    }
}
