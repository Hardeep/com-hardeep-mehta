var contact = angular.module('contact', []);

// create angular controller
contact.controller('mainController', function($scope, $http) {

    $scope.emailsent = false;

    // process the form
    $scope.submitForm = function() {

        // request object
        var request = $http({
            method: "post",
            url: "/sendmail",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: $.param({
                firstname: $scope.firstname,
                lastname: $scope.lastname,
                emailaddress: $scope.emailaddress,
                comments: $scope.comments
            })
        })

        request.success(function(response, data) {
            console.log("'"+response+"'");
            if (response == "OK") { // if OK, email has been sent, update the view
                $scope.emailsent = true;
                console.log("Email was sent...");
            } else {
            	console.log("Response is NOT OK");
            }
        });

    };

});