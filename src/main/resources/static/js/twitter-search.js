var formaApp = angular.module('hmehtaTwitterSearch', []);

formaApp.controller('hmehtaTwitterSearchCtrl', function($scope, $http) {

	$scope.doSearch = function() {

		var hashtag = $scope.hashtag;
		if (hashtag == undefined) {
			hashtag = "newyork";
		}
		var url = "/twittersearch?hashtag=" + hashtag;
		$http.get(url).success(function(response, data) {
			var jsonTweets = response.statuses;
			var updatedTweets = [];
			for (var i = 0; i < jsonTweets.length; i++) {
				var tweet = jsonTweets[i];
				tweet.text = parseTwitter(tweet.text);
				updatedTweets[i] = tweet;
			}
			$scope.tweets = updatedTweets;
			console.log(updatedTweets);
		}).error(function(data, status) {
			console.log('ERROR [%s]', data);
		});
	}

});

formaApp.directive('bindUnsafeHtml', [ '$compile', function($compile) {
	return function(scope, element, attrs) {
		scope.$watch(function(scope) {
			return scope.$eval(attrs.bindUnsafeHtml);
		}, function(value) {
			element.html(value);
			$compile(element.contents())(scope);
		});
	};
} ]);

function parseTwitter(text) {
	// Parse URIs
	text = text.replace(
			/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&\?\/.=]+/, function(
					uri) {
				return uri.link(uri);
			});

	// Parse Twitter usernames
	text = text.replace(/[@]+[A-Za-z0-9-_]+/, function(u) {
		var username = u.replace("@", "")
		return u.link("http://twitter.com/" + username);
	});

	// Parse Twitter hash tags
	text = text.replace(/[#]+[A-Za-z0-9-_]+/, function(t) {
		var tag = t.replace("#", "%23")
		return t.link("https://twitter.com/search?q=" + tag);
	});
	return text;
}