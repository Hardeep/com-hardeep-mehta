package com.hmehta.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebController {

	@RequestMapping("/")
	public String index(ModelAndView modelAndView) {
		return "index";
	}

	@RequestMapping("/contact/")
	public String contact(ModelAndView modelAndView) {
		return "contact/index";
	}

	@RequestMapping("/projects/")
	public String projects(ModelAndView modelAndView) {
		return "projects/index";
	}

	@RequestMapping("/projects/aws-lambda-sms-microservice/")
	public String awsLambdaSMS(ModelAndView modelAndView) {
		return "projects/aws-lambda-sms-microservice/index";
	}

	@RequestMapping("/projects/aws-lambda-image-processor-microservice/")
	public String awsLambdaImageResize(ModelAndView modelAndView) {
		return "projects/aws-lambda-image-processor-microservice/index";
	}

	@RequestMapping("/projects/state-facts/")
	public String stateFacts(ModelAndView modelAndView) {
		return "projects/state-facts/index";
	}

	@RequestMapping("/projects/twitter-search/")
	public String twitterSearch(ModelAndView modelAndView) {
		return "projects/twitter-search/index";
	}

}