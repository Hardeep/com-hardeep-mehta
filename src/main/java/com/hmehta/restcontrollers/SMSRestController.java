package com.hmehta.restcontrollers;

import javax.servlet.http.HttpServletRequest;
import com.hmehta.config.HardeepMehtaConfiguration;
import org.springframework.web.client.RestTemplate;
import com.hmehta.service.impl.VerifyRecapthchaServiceImpl;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

/**
 * Send an SMS text message to any number in the Unitied States using the AWS
 * Lambda function.
 * 
 * @author HMehta
 *
 */
@RestController
@EnableAutoConfiguration
public class SMSRestController extends AbstractRestController {

    @Autowired
    private VerifyRecapthchaServiceImpl service;

    @Autowired
    private HardeepMehtaConfiguration   configuration;

    @RequestMapping(value = "/sendsms", method = RequestMethod.POST, produces = { "application/json" })
    public String sendsms(@RequestParam(name = "cellPhoneNumber", required = true) String cellPhoneNumber, @RequestParam(name = "smsText", required = true) String smsText,
            @RequestParam(name = "serviceProvider", required = true) String serviceProvider, @RequestParam(name = "g-recaptcha-response", required = true) String recaptchaResponse,
            HttpServletRequest request) {

        String result = "{}";
        if (service.isUserVerified(recaptchaResponse, request.getRemoteAddr())) {
            restLogger.info("Calling AWS Lambda function to send the SMS text with cellPhoneNumer=" + cellPhoneNumber + ", serviceProvider=" + serviceProvider + ", smsText={" + smsText + "}");
            String smsServiceURL = configuration.getAwsLambdaSmsProcessorFunctionUrl() + "?cellPhoneNumber=" + cellPhoneNumber + "&serviceProvider=" + serviceProvider + "&smsText=" + smsText;
            result = new RestTemplate().getForObject(smsServiceURL, String.class);
        }

        return result;
    }

}
