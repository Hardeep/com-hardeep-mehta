var hmStateFactsApp = angular.module('hmStateFacts', []);

hmStateFactsApp.controller('hmStateFactsCtrl', function($scope, $http,
		$filter, SplitArrayService) {

	$scope.$watch('search', function(val) {

		$http.get('/data/state_facts.json').success(function(data) {
			if (val) {
				var states = $filter('filter')(data, {
					STATE : val
				});
				$scope.states = SplitArrayService.SplitArray(states, 3);
			} else {
				$scope.states = SplitArrayService.SplitArray(data, 3);
			}
		});

	});

});

hmStateFactsApp.service('SplitArrayService', function() {
	return {
		SplitArray : function(array, size) {
			var newArr = [];
			for (var i = 0; i < array.length; i += size) {
				newArr.push(array.slice(i, i + size));
			}
			return newArr;
		}
	}
});
