package com.hmehta.restcontrollers;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import com.hmehta.config.AWSConfiguration;
import javax.mail.internet.InternetAddress;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@RestController
@EnableAutoConfiguration
public class SendMailRestController extends AbstractRestController {

	private static final int PORT = 587;

	private static final String TO_FROM_EMAIL = "hmehta.email@gmail.com";
	private static final String HOST = "email-smtp.us-east-1.amazonaws.com";

	@Autowired
	private AWSConfiguration configuration;

	@RequestMapping(value = "/sendmail", method = RequestMethod.POST)
	public String sendMail(@RequestParam("firstname") String firstName, @RequestParam("lastname") String lastName, @RequestParam("emailaddress") String emailAddress,
			@RequestParam("comments") String comments) {

		String from = firstName + " " + lastName + "<" + emailAddress + ">";
		String subject = "Inquiry from " + from;

		Properties properties = System.getProperties();
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.port", PORT);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.starttls.required", "true");

		Session mailSession = Session.getDefaultInstance(properties);
		Transport transport = null;
		try {
			MimeMessage message = new MimeMessage(mailSession);
			message.setFrom(new InternetAddress(TO_FROM_EMAIL));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(TO_FROM_EMAIL));
			message.setSubject(subject);
			InternetAddress[] addresses = new InternetAddress[1];
			addresses[0] = new InternetAddress(from);
			message.setReplyTo(addresses);
			message.setContent(comments, "text/plain");
			transport = mailSession.getTransport();
			restLogger.info("Sending email via SES SMTP interface. Port=" + PORT + "...");
			transport.connect(HOST, configuration.getSmtpUsername(), configuration.getSmtpPassword());
			transport.sendMessage(message, message.getAllRecipients());
			restLogger.info("Email sent.");
			return "OK";
		} catch (MessagingException mex) {
			restLogger.error("Exception closing the Transport." + mex.toString());
			return "ERROR";
		} finally {
			try {
				if (transport != null) {
					transport.close();
				}
			} catch (MessagingException mex) {
				restLogger.error("Exception closing the Transport." + mex.toString());
			}
		}
	}
}